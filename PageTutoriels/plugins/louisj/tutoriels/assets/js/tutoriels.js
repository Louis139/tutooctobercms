
// Handles copy of specified message to clipboard
function copyToClipboard(message) {
    let $v_tmpInput = document.createElement('TEXTAREA');
    let $v_clipboardContainer = $('body')[0];
    $v_clipboardContainer.appendChild($v_tmpInput);
    $v_tmpInput.value=message;
    $v_tmpInput.select();
    document.execCommand('copy');
    $v_clipboardContainer.removeChild($v_tmpInput);
}

// Clears checked elements in listwidgets
/*function clearChecked($p_btns) {
    $p_btns.downloadZip.prop('disabled', true);
    $p_btns.delete.prop('disabled', true);
    $('[data-control="listwidget"] table').find('tr').removeClass('active');
    $('.list-checkbox input[type="checkbox"]').prop('checked', false)
}*/

class DocumentColumn {
    //##################################################################################################################
    // Constant(s)
    //##################################################################################################################

    //##################################################################################################################
    // Constructor
    //##################################################################################################################
    constructor(p_container, p_webPath) {
        this.init(p_container, p_webPath);
    }

    //##################################################################################################################
    // Constant(s)
    //##################################################################################################################

    init(p_container, p_webPath) {
        this.$_container = $(p_container);
        this.$_container.removeClass('nolink');
        this._webPath = p_webPath;
        this.bindEvents();
    }

    clear() {
        this.$_container.removeClass('active');
    }

    bindEvents() {
        let v_column = this;
        this.$_container.on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            window.location.href=v_column._webPath;
        });
    }

}

class Document {
    //##################################################################################################################
    // Constant(s)
    //##################################################################################################################



    //##################################################################################################################
    // Constructor
    //##################################################################################################################

    constructor(p_container) {
        this.init(p_container);
    }

    //##################################################################################################################
    // Method(s)
    //##################################################################################################################

    get isDocument() {
        return this.$_container.children().first().is('td');
    }

    decorate() {
        if(this.isDocument)
            this.$_container.addClass('rowlink');
    }

    init(p_container) {
        this.$_container = $(p_container);
        this.$_checkbox = this.$_container.find('input[type="checkbox"]');
        this._columns = [];
        this._urls = {};
        let v_document = this;
        let v_columns = this.$_container.find('td');
        let v_columnsLast = this.getLastColumnId(v_columns);
        this._urls.webPath = $(v_columns[v_columnsLast]).find('[manage-model="document"]').attr('web-path');
        this.$_actions = {
            copy: $(v_columns[v_columnsLast]).find('[c-bind="url-copy"]')
        };
        v_columns.each(function(i) {
            if(i > 0 && i < v_columnsLast) {
                v_document._columns.push(new DocumentColumn(v_columns[i], v_document._urls.webPath));
            } else {
                $(v_columns[i]).addClass('nolink');
            }
        });
        this.decorate();
        this.bindEvents();
    }

    clear() {
        this.$_container.removeClass('active');
        this.$_checkbox.prop('checked', false);
    }

    getLastColumnId(p_columns) {
        if(!$(p_columns[p_columns.length-1]).hasClass('list-setup')) return p_columns.length-1;
        else return p_columns.length-2;
    }

    bindEvents() {
        let v_document = this;
        this.$_actions.copy.on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            copyToClipboard(v_document.$_actions.copy.attr('c-url'));
        });
    }

}

class DocumentListToolbar {
    //##################################################################################################################
    // Constant(s)
    //##################################################################################################################

    static get SELECTORS() {
        return {
            listToolbar: '[data-control="toolbar"]',
            btnDelete: '[c-bind="delete"]',
            btnDownloadZip: '[c-bind="downloadZip"]'
        };
    }

    //##################################################################################################################
    // Constructor
    //##################################################################################################################

    constructor(p_mode = DocumentList.MODES.self) {
        this.init(p_mode);
    }

    //##################################################################################################################
    // Method(s)
    //##################################################################################################################

    //------------------------------------------------------------------------------------------------------------------
    // Helpers
    //------------------------------------------------------------------------------------------------------------------

    get downloadZipUrlBase() {
        return this.$_buttons.downloadZip.attr('c-url');
    }

    //------------------------------------------------------------------------------------------------------------------
    // Helpers
    //------------------------------------------------------------------------------------------------------------------

    init(p_mode = DocumentList.MODES.self) {
        switch(p_mode) {
            default:
                this.$_container = $(DocumentListToolbar.SELECTORS.listToolbar);
                break;
        }
        this.$_buttons = {
            downloadZip: this.$_container.find(DocumentListToolbar.SELECTORS.btnDownloadZip),
            delete: this.$_container.find(DocumentListToolbar.SELECTORS.btnDelete)
        };
    }

    getDownloadZipUrl(p_checked) {
        return this.downloadZipUrlBase.replace(
            '%ids;',
            p_checked
        )
    }

    clearButtonsState() {
        for(let i in this.$_buttons) {
            this.$_buttons[i].prop('disabled', true);
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    // Events
    //------------------------------------------------------------------------------------------------------------------

    bindDownload(p_documentList) {
        let v_toolbar = this;
        this.$_buttons.downloadZip.on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            window.open(
                v_toolbar.getDownloadZipUrl(p_documentList.checked)
            );
            v_toolbar.clearButtonsState();
            p_documentList.clearChecked();
        });
    }
}

class DocumentList {
    //##################################################################################################################
    // Constant(s)
    //##################################################################################################################

    /* List of modes :
     * {
     *     'object' => Documents list embedded as relation in object update form,
     *     'group' => Documents list embedded as relation in group update form,
     *     'reminder' => Documents list embedded as relation in reminder update form,
     *     'self' => Crud document list
     * }
     */

    static get MODES() {
        return {
            self: 1
        }
    }

    static get SELECTORS() {
        return {
            listWidget: '[data-control="listwidget"]'
        }
    }

    //##################################################################################################################
    // Constructor
    //##################################################################################################################

    constructor() {
        this.init(DocumentList.findCurrentMode());
    }

    //##################################################################################################################
    // Accessor(s)
    //##################################################################################################################

    get checked() {
        return this.$_container.listWidget('getChecked').join(',');
    }

    //##################################################################################################################
    // Method(s)
    //##################################################################################################################

    //------------------------------------------------------------------------------------------------------------------
    // Helpers
    //------------------------------------------------------------------------------------------------------------------

    static findCurrentMode() {
        let v_mode = DocumentList.MODES.self;
        return v_mode;
    }

    init(p_mode= DocumentList.MODES.self) {
        this._currentMode = p_mode;
        switch(this._currentMode) {
            default:
                this.$_container = $(DocumentList.SELECTORS.listWidget);
                break;
        }
        this.$_parent = this.$_container.parent().parent();

        // Adding rows
        this._rows = [];
        this.collectRows();
        this.bindRowsUpdate();

        // Adding toolbar
        this._toolbar = null;
        this._toolbar = new DocumentListToolbar(this._currentMode);
        this._toolbar.bindDownload(this);
    }

    clearChecked() {
        for(let i in this._rows) {
            if(this._rows.hasOwnProperty(i)) {
                this._rows[i].clear();
            }
        }
    }

    collectRows() {
        let v_documentList = this;
        let $v_rows = this.$_container.find('tr');
        console.log(this.$_container);
        $v_rows.each(function(i) {
            v_documentList._rows.push(new Document($v_rows[i]));
        });
    }

    //------------------------------------------------------------------------------------------------------------------
    // Events
    //------------------------------------------------------------------------------------------------------------------

    bindRowClick() {

    }

    bindRowsUpdate() {
        let v_documentsList = this;
        this.$_parent.on('ajaxUpdate', function(event, context) {
            v_documentsList.init(v_documentsList._currentMode);
        });
    }
}


$(document).ready(function() {
    v_documentList = new DocumentList();
});
