<?php return [
    'plugin' => [
        'name' => 'Tutoriels',
        'description' => 'Plugin permettant d\'ajouter des tutoriels écrits ou vidéos avec un type et une catégorie.'
    ]
];