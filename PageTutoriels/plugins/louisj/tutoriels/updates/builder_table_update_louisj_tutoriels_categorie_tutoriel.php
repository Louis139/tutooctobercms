<?php namespace louisJ\Tutoriels\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLouisjTutorielsCategorieTutoriel extends Migration
{
    public function up()
    {
        Schema::table('louisj_tutoriels_categorie_tutoriel', function($table)
        {
    
            $table->renameColumn('type_id', 'tutoriel_id');
        });
    }
    
    public function down()
    {
        Schema::table('louisj_tutoriels_categorie_tutoriel', function($table)
        {
    
            $table->renameColumn('tutoriel_id', 'type_id');
        });
    }
}
