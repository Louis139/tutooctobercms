<?php namespace louisJ\Tutoriels\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLouisjTutorielsTutoriels3 extends Migration
{
    public function up()
    {
        Schema::table('louisj_tutoriels_tutoriels', function($table)
        {
    
            $table->text('description')->default('null')->change();
            $table->boolean('published')->nullable()->change();
    
        });
    }
    
    public function down()
    {
        Schema::table('louisj_tutoriels_tutoriels', function($table)
        {
    
            $table->text('description')->default('NULL')->change();
            $table->boolean('published')->nullable(false)->change();
        });
    }
}
