<?php namespace louisJ\Tutoriels\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLouisjTutorielsCategorieTutoriel extends Migration
{
    public function up()
    {
        Schema::create('louisj_tutoriels_categorie_tutoriel', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('tutoriel_id')->unsigned();
            $table->integer('categorie_id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            
            $table->foreign('tutoriel_id')
                    ->references('id')
                    ->on('louisj_tutoriels_tutoriels')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
    
                $table->foreign('categorie_id')
                    ->references('id')
                    ->on('louisj_tutoriels_categorie')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
    
        });
    }
    
    public function down()
    {
        Schema::table('louisj_tutoriels_categorie_tutoriel', function(Blueprint $table) {
                //Drop foreign keys before deleting table
                $table->dropForeign('louisj_tutoriels_categorie_tutoriel_tutoriel_id_foreign');
                $table->dropForeign('louisj_tutoriels_categorie_tutoriel_categorie_id_foreign');
            });
    
        Schema::dropIfExists('louisj_tutoriels_categorie_tutoriel');
    }
}
