<?php namespace louisJ\Tutoriels\Updates;

use October\Rain\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLouisjTutorielsFiltreTutoriel2 extends Migration
{
    public function up()
    {
        Schema::table('louisj_tutoriels_filtre_tutoriel', function($table)
        {

        $table->foreign('tutoriel_id')
                        ->references('id')
                        ->on('louisj_tutoriels_tutoriels')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');

                    $table->foreign('filtre_id')
                        ->references('id')
                        ->on('louisj_tutoriels_filtre')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');

        });
    }

    public function down()
    {
        Schema::table('louisj_tutoriels_filtre_tutoriel', function(Blueprint $table) {
                    //Drop foreign keys before deleting table
                    $table->dropForeign('louisj_tutoriels_filtre_tutoriel_tutoriel_id_foreign');
                    $table->dropForeign('louisj_tutoriels_filtre_tutoriel_filtre_id_foreign');
                });
    }
}
