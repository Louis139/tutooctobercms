<?php namespace louisJ\Tutoriels\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLouisjTutorielsTypes2 extends Migration
{
    public function up()
    {
        Schema::table('louisj_tutoriels_types', function($table)
        {
    
            $table->text('description')->default('null')->change();
            $table->dropColumn('filtre');
        });
    }
    
    public function down()
    {
        Schema::table('louisj_tutoriels_types', function($table)
        {
    
            $table->text('description')->default('NULL')->change();
            $table->string('filtre', 191)->nullable()->default('NULL');
        });
    }
}
