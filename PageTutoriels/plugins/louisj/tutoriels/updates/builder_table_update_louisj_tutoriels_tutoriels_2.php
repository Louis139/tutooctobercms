<?php namespace louisJ\Tutoriels\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLouisjTutorielsTutoriels2 extends Migration
{
    public function up()
    {
        Schema::table('louisj_tutoriels_tutoriels', function($table)
        {
            $table->dateTime('date')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('louisj_tutoriels_tutoriels', function($table)
        {
            $table->dropColumn('date');
        });
    }
}
