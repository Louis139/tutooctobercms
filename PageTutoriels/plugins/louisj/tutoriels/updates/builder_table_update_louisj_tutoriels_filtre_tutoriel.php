<?php namespace louisJ\Tutoriels\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLouisjTutorielsFiltreTutoriel extends Migration
{
    public function up()
    {
        Schema::rename('louisj_tutoriels_categorie_tutoriel', 'louisj_tutoriels_filtre_tutoriel');
        Schema::table('louisj_tutoriels_filtre_tutoriel', function($table)
        {
    
            $table->renameColumn('categorie_id', 'filtre_id');
        });
    }
    
    public function down()
    {
        Schema::rename('louisj_tutoriels_filtre_tutoriel', 'louisj_tutoriels_categorie_tutoriel');
        Schema::table('louisj_tutoriels_categorie_tutoriel', function($table)
        {
    
            $table->renameColumn('filtre_id', 'categorie_id');
        });
    }
}
