<?php namespace louisJ\Tutoriels\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLouisjTutorielsFiltre extends Migration
{
    public function up()
    {
        Schema::rename('louisj_tutoriels_categorie', 'louisj_tutoriels_filtre');
        Schema::table('louisj_tutoriels_filtre', function($table)
        {
    
            $table->text('description')->default('null')->change();
        });
    }
    
    public function down()
    {
        Schema::rename('louisj_tutoriels_filtre', 'louisj_tutoriels_categorie');
        Schema::table('louisj_tutoriels_categorie', function($table)
        {
    
            $table->text('description')->default('NULL')->change();
        });
    }
}
