<?php namespace louisj\tutoriels\Updates;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use October\Rain\Database\Updates\Seeder;
use louisj\tutoriels\Models\Type;

class SeedType extends Seeder
{
    public function run()
    {
        Db::table('louisj_tutoriels_types')->insert([
            'nom' => 'Vidéo',
            'description' => 'Tutoriel de type vidéo uniquement',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        Db::table('louisj_tutoriels_types')->insert([
            'nom' => 'Ecrits',
            'description' => 'Tutoriel rédigé via interface web',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        Db::table('louisj_tutoriels_types')->insert([
            'nom' => 'Avec Documents',
            'description' => 'Tutoriel avec des pièces jointes',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

    }
}
