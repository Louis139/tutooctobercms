<?php namespace louisJ\Tutoriels\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLouisjTutorielsTutoriels extends Migration
{
    public function up()
    {
        Schema::create('louisj_tutoriels_tutoriels', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('nom');
            $table->string('auteur');
            $table->text('description')->nullable();
            $table->string('slug');
            $table->integer('type_id')->unsigned();
            
            $table->foreign('type_id')
                    ->references('id')
                    ->on('louisj_tutoriels_types')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
    
        });
    }
    
    public function down()
    {
        Schema::table('louisj_tutoriels_tutoriels', function(Blueprint $table) {
                //Drop foreign keys before deleting table
                $table->dropForeign('louisj_tutoriels_tutoriels_type_id_foreign');
            });
    
        Schema::dropIfExists('louisj_tutoriels_tutoriels');
    }
}
