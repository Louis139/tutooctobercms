<?php namespace louisJ\Tutoriels\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLouisjTutorielsTypes extends Migration
{
    public function up()
    {
        Schema::table('louisj_tutoriels_types', function($table)
        {
            $table->string('filtre')->nullable();
    
        });
    }
    
    public function down()
    {
        Schema::table('louisj_tutoriels_types', function($table)
        {
            $table->dropColumn('filtre');
    
        });
    }
}
