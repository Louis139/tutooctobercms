<?php namespace louisJ\Tutoriels\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLouisjTutorielsTutoriels extends Migration
{
    public function up()
    {
        Schema::table('louisj_tutoriels_tutoriels', function($table)
        {
            $table->boolean('published')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('louisj_tutoriels_tutoriels', function($table)
        {
            $table->dropColumn('published');
        });
    }
}
