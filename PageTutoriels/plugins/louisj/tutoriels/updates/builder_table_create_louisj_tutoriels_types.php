<?php namespace louisJ\Tutoriels\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateLouisjTutorielsTypes extends Migration
{
    public function up()
    {
        Schema::create('louisj_tutoriels_types', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->string('nom');
            $table->text('description')->nullable()->default('null');
        });
    }

    public function down()
    {
        Schema::dropIfExists('louisj_tutoriels_types');
    }
}
