<?php namespace louisJ\Tutoriels\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateLouisjTutorielsTutoriels4 extends Migration
{
    public function up()
    {
        Schema::table('louisj_tutoriels_tutoriels', function($table)
        {
            $table->text('contenu')->nullable()->default('null');
            $table->text('description')->default('null')->change();
        });
    }
    
    public function down()
    {
        Schema::table('louisj_tutoriels_tutoriels', function($table)
        {
            $table->dropColumn('contenu');
            $table->text('description')->default('NULL')->change();
        });
    }
}
