<?php namespace louisJ\Tutoriels\Models;

use Model;

/**
 * Model
 */
class Filtre extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'louisj_tutoriels_filtre';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsToMany = [
        'tutoriels' => [
            'louisj/Tutoriels/Models/tutoriel',
            'table' => 'louisj_tutoriels_filtre_tutoriel',
            'key' => 'filtre_id',
            'otherKey' => 'tutoriel_id'
        ],
    ];

}
