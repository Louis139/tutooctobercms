<?php namespace louisJ\Tutoriels\Models;

use Model;
use October\Tester\Components\Categories;

/**
 * Model
 */
class Tutoriel extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'louisj_tutoriels_tutoriels';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachOne = [
        'document' => 'System\Models\File'
    ];

    public $belongsTo = [
        'type' => [
            Type::class,
            'key' => 'type_id'
        ]
    ];

    public $belongsToMany = [
        'filtres' => [
            Filtre::class,
            'table' => 'louisj_tutoriels_filtre_tutoriel',
            'key' => 'tutoriel_id',
            'otherKey' => 'filtre_id'
        ],
    ];

}
