<?php namespace louisJ\Tutoriels\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use louisJ\Tutoriels\Models\Tutoriel;
use Exception;

class Tutoriels extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ReorderController',

    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';
    public $recordId = null;


    public function __construct()
    {
        $this->fetchId();
        if(!is_null($this->recordId)) {
            $tuto = Tutoriel::find($this->recordId);
            switch ($tuto->type->nom):
                default:
                    break;
                case "Vidéo":
                    $this->formConfig = 'config_form_type_videos.yaml';
                    break;
                case "Avec Documents":
                    $this->formConfig = 'config_form_type_doc.yaml';
                    break;
                case "Ecrits":
                    $this->formConfig = 'config_form_type_ecrits.yaml';
                    break;
            endswitch;
        }

        parent::__construct();
        BackendMenu::setContext('louisJ.Tutoriels', 'Tutoriels-et-Docs', 'Tutoriels');
        $this->addJs('/plugins/louisj/tutoriels/assets/js/tutoriels.js');
    }

    public function fetchId() {

        try {
            $this->recordId = explode('/',Request()->getRequestUri())[6];
        }catch (Exception $e) {

        }
    }

    public function preview($id = null)
    {
        $this->vars['record'] = Tutoriel::with('document')->where('id', $id)->first();
        return parent::preview($id);
    }

}
