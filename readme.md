Tutoriel sur les bases d'October CMS dans le but d'apprendre pour un plus gros projet.
Ce git comporte une maquette pour ajouter des Docs et Tutoriels sur un site déjà existant basé sur October CMS.
Il comporte aussi tout le code nécessaire à la réalisation du site.
Realisé par Louis Jullien.
Edit: Pour retrouver le plugin:
-> PageTutoriels -> plugins -> louisj/tutoriels
